import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

class UserClient {
    private String userId;
    private BufferedReader reader;
    private PrintWriter writer;

    public UserClient(String userId, BufferedReader reader, PrintWriter writer) {
        this.userId = userId;
        this.reader = reader;
        this.writer = writer;
    }

    public void start() {
        new Thread(new ServerListener()).start();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome, " + userId + "!");
        while (true) {
            System.out.println("1. Send Message");
            System.out.println("2. Display Messages");
            System.out.println("3. Exit");
            System.out.print("Choose an option: ");
            int option = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            switch (option) {
                case 1:
                    sendMessage(scanner);
                    break;
                case 2:
                    displayMessages();
                    break;
                case 3:
                    System.out.println("Exiting...");
                    System.exit(0);
            }
        }
    }

    private void sendMessage(Scanner scanner) {
        System.out.print("Enter receiver's ID (or 'exit' to quit): ");
        String receiverId = scanner.nextLine();
        if (receiverId.equalsIgnoreCase("exit")) {
            System.out.println("Exiting...");
            System.exit(0);
        }

        System.out.print("Enter message: ");
        String message = scanner.nextLine();

        writer.println(userId + " -> " + receiverId + ": " + message);
    }

    private void displayMessages() {
        System.out.println("Your Messages:");
        writer.println(userId); // Request messages from the server
    }

    private class ServerListener implements Runnable {
        @Override
        public void run() {
            try {
                String message;
                while ((message = reader.readLine()) != null) {
                    System.out.println(message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
