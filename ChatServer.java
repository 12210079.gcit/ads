import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;

public class ChatServer {
    private ServerSocket serverSocket;
    private Map<String, PrintWriter> connectedClients;

    public ChatServer(int port) {
        connectedClients = new HashMap<>();
        try {
            serverSocket = new ServerSocket(port);
            System.out.println("Server is running on port " + port);
            acceptClients();
        } catch (IOException e) {
            e.printStackTrace();
        }
        UserGraph userGraph = new UserGraph("user_graph.txt", "user_messages.txt", connectedClients);
        userGraph.setConnectedClients(connectedClients);

    }

    private void acceptClients() {
        while (true) {
            try {
                Socket clientSocket = serverSocket.accept();
                System.out.println("New client connected: " + clientSocket);

                // Create a new thread to handle the client
                ClientHandler clientHandler = new ClientHandler(clientSocket);
                new Thread(clientHandler).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private class ClientHandler implements Runnable {
        private Socket clientSocket;
        private BufferedReader reader;
        private PrintWriter writer;
        private String username;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new PrintWriter(clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                // Get the username from the client
                username = reader.readLine();
                System.out.println("New user joined: " + username);

                // Add the client's PrintWriter to the connectedClients map
                connectedClients.put(username, writer);

                // Broadcast the new user to all clients
                broadcastMessage(username + " has joined the chat.");

                // Listen for messages from the client and broadcast them to all clients
                String message;
                while ((message = reader.readLine()) != null) {
                    broadcastMessage(username + ": " + message);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // Remove the client when they disconnect
                connectedClients.remove(username);
                broadcastMessage(username + " has left the chat.");

                try {
                    reader.close();
                    writer.close();
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private void broadcastMessage(String message) {
            // Broadcast the message to all connected clients
            for (PrintWriter clientWriter : connectedClients.values()) {
                clientWriter.println(message);
            }
        }
    }

    public static void main(String[] args) {
        int port = 8083; // Change this to your desired port
        new ChatServer(port);
    }
}
