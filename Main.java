
// V2
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Map<String, PrintWriter> connectedClients = new HashMap<>();

        // Create an instance of UserAccount with connectedClients
        UserAccount userAccount = new UserAccount(connectedClients);
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("1. Login");
            System.out.println("2. Signup");
            System.out.print("Choose an option: ");
            int option = scanner.nextInt();
            scanner.nextLine(); // Consume newline

            switch (option) {
                case 1:
                    login(userAccount, scanner);
                    break;
                case 2:
                    signup(userAccount, scanner);
                    break;
                default:
                    System.out.println("Invalid option. Try again.");
                    break;
            }
        }
    }

    private static void login(UserAccount userAccount, Scanner scanner) {
        System.out.print("Enter username: ");
        String username = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();

        if (userAccount.login(username, password)) {
            userAccount.setCurrentUsername(username);
            System.out.println("Login successful!");
            // userAccount.handleIncomingMessages();

            // Show registered users (excluding current user and connections)
            Set<String> registeredUsers = userAccount.getRegisteredUsers();
            registeredUsers.remove(username);
            registeredUsers.removeAll(userAccount.getConnectedUsers());
            System.out.println("Registered Users: " + registeredUsers);

            // Ask if the user wants to add connections
            System.out.print("Do you want to add connections? (yes/no): ");
            String addConnections = scanner.nextLine().toLowerCase();
            if (addConnections.equals("yes")) {
                addConnections(userAccount, scanner);
            }

            // Display connected users
            Set<String> connectedUsers = userAccount.getConnectedUsers();
            System.out.println("Connected Users: " + connectedUsers);

            // Provide options for starting a chat and logging out
            while (true) {
                System.out.println("1. Start Chat");
                System.out.println("2. Logout");
                System.out.print("Choose an option: ");
                int chatOption = scanner.nextInt();
                scanner.nextLine(); // Consume newline

                switch (chatOption) {
                    case 1:
                        startChat(userAccount, scanner);
                        break;
                    case 2:
                        // Add logic for logout
                        System.out.println("Logging out...");
                        return; // exit the method
                    default:
                        System.out.println("Invalid option. Try again.");
                        break;
                }
            }
        } else {
            System.out.println("Login failed. Please check your credentials.");
        }
    }

    private static void startChat(UserAccount userAccount, Scanner scanner) {
        // Display user's connections
        Set<String> connectedUsers = userAccount.getConnectedUsers();
        System.out.println("Your Connections: " + connectedUsers);

        // Ask the user to choose a friend to chat with
        System.out.print("Choose a friend to chat with: ");
        String friendToChat = scanner.nextLine();

        // Check if the selected friend is a valid connected user
        if (connectedUsers.contains(friendToChat)) {
            // Display messages between the current user and the selected friend
            String[] messages = userAccount.receiveMessages(friendToChat);
            for (String message : messages) {
                // Check if the message is from the selected friend
                if (message.startsWith(friendToChat + ":")) {
                    System.out.println("Received: " + message);
                }
            }

            // Ask the user to enter the chat message
            System.out.print("Enter your message: ");
            String chatMessage = scanner.nextLine();

            // Send the private chat message to the selected friend
            userAccount.sendMessage(userAccount.getCurrentUsername(), friendToChat, chatMessage);

        } else {
            System.out.println("Invalid friend selected or not connected.");
        }
    }

    private static void signup(UserAccount userAccount, Scanner scanner) {
        System.out.print("Enter username: ");
        String username = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();

        if (userAccount.signup(username, password)) {
            System.out.println("Signup successful!");
        } else {
            System.out.println("Signup failed. Please choose another username.");
        }
    }

    private static void addConnections(UserAccount userAccount, Scanner scanner) {
        System.out.print("Enter the username you want to connect with: ");
        String otherUser = scanner.nextLine();

        if (userAccount.addConnection(userAccount.getCurrentUsername(), otherUser)) {
            System.out.println("Connection added successfully!");
        } else {
            System.out.println("Failed to add connection. Please check the username.");
        }
    }
}
