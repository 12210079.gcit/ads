
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class UserAccount {
    private static final String ACCOUNTS_FILE = "user_accounts.txt";
    private Map<String, String> accounts;
    private String currentUsername;
    private UserGraph userGraph; // Declare a UserGraph instance
    private Map<String, PrintWriter> connectedClients; // Declare connectedClients
    private boolean messagesReceived = false;

    public UserAccount(Map<String, PrintWriter> connectedClients) {
        this.connectedClients = connectedClients;
        this.accounts = loadAccounts();
        this.userGraph = new UserGraph("user_graph.txt", "user_messages.txt", connectedClients);
    }

    public void sendMessage(String senderId, String receiverId, String message) {
        userGraph.sendMessage(senderId, receiverId, message);
    }

    private Map<String, String> loadAccounts() {
        try (BufferedReader reader = new BufferedReader(new FileReader(ACCOUNTS_FILE))) {
            Map<String, String> accounts = new HashMap<>();
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(":");
                String username = parts[0].trim();
                String password = parts[1].trim();
                accounts.put(username, password);
            }
            return accounts;
        } catch (FileNotFoundException e) {
            return new HashMap<>();
        } catch (IOException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    private void saveAccounts() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(ACCOUNTS_FILE))) {
            for (Map.Entry<String, String> entry : accounts.entrySet()) {
                String line = entry.getKey() + ": " + entry.getValue();
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean signup(String username, String password) {
        if (!accounts.containsKey(username)) {
            accounts.put(username, password);
            saveAccounts();
            return true;
        } else {
            System.out.println("Username already exists. Please choose another username.");
            return false;
        }
    }

    public boolean login(String username, String password) {
        return accounts.containsKey(username) && accounts.get(username).equals(password);
    }

    public String getCurrentUsername() {
        return currentUsername;
    }

    public void setCurrentUsername(String username) {
        this.currentUsername = username;
    }

    public Set<String> getRegisteredUsers() {
        return accounts.keySet();
    }

    public Set<String> getConnectedUsers() {
        // Retrieve connected users from the shared UserGraph instance
        return userGraph.getConnectedUsers(currentUsername);
    }

    public boolean addConnection(String user1, String user2) {
        if (accounts.containsKey(user2) && !user1.equals(user2)) {
            // Check if the users are already connected using the shared UserGraph instance
            if (!userGraph.isConnected(user1, user2)) {
                // Add connection to the shared UserGraph instance
                userGraph.addConnection(user1, user2);
                return true;
            } else {
                System.out.println("Users are already connected.");
            }
        } else {
            System.out.println("Invalid username or self-connection.");
        }
        return false;
    }

    public String[] receiveMessages(String userId) {
        // Retrieve and return the messages for the specified user
        return userGraph.receiveMessages(userId);
    }

    public void handleIncomingMessages() {
        if (messagesReceived) {
            System.out.println("Messages already received. Exiting...");
            return;
        }

        Thread messageListener = new Thread(() -> {
            String[] messages = receiveMessages(currentUsername);
            for (String message : messages) {
                // Process and display the incoming message
                System.out.println("Received: " + message);
            }

            // Set the messagesReceived flag to true to indicate that messages have been
            // received
            messagesReceived = true;
        });

        messageListener.start();
    }

}
