
// displaying already connected users 
import java.io.*;
import java.util.*;

class UserGraph {
    private Map<String, CustomLinkedList> adjacencyList;
    private Map<String, CustomLinkedList> messages;
    private Set<Connection> connections; // Add this set to store connections
    private String graphFilePath;
    private String messagesFilePath;
    private Map<String, PrintWriter> connectedClients; // Add this field

    public UserGraph(String graphFilePath, String messagesFilePath, Map<String, PrintWriter> connectedClients) {
        this.graphFilePath = graphFilePath;
        this.messagesFilePath = messagesFilePath;
        this.adjacencyList = loadGraph();
        this.messages = loadMessages();
        this.connections = new HashSet<>();
        this.connectedClients = connectedClients; // Initialize connectedClients
    }

    private static class Connection {
        String user1;
        String user2;

        public Connection(String user1, String user2) {
            this.user1 = user1;
            this.user2 = user2;
        }

        // Override equals and hashCode methods
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null || getClass() != obj.getClass())
                return false;
            Connection connection = (Connection) obj;
            return Objects.equals(user1, connection.user1) && Objects.equals(user2, connection.user2);
        }

        @Override
        public int hashCode() {
            return Objects.hash(user1, user2);
        }
    }

    public void setConnectedClients(Map<String, PrintWriter> connectedClients) {
        this.connectedClients = connectedClients;
    }

    private Map<String, CustomLinkedList> loadGraph() {
        try (BufferedReader reader = new BufferedReader(new FileReader(graphFilePath))) {
            Map<String, CustomLinkedList> graph = new HashMap<>();
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(":");
                String userId = parts[0].trim();

                if (parts.length > 1) {
                    String[] friends = parts[1].split(",");
                    CustomLinkedList friendsList = new CustomLinkedList();
                    for (String friend : friends) {
                        friendsList.add(friend.trim());
                    }
                    graph.put(userId, friendsList);
                } else {
                    graph.put(userId, new CustomLinkedList());
                }
            }
            return graph;
        } catch (FileNotFoundException e) {
            return new HashMap<>();
        } catch (IOException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    private Map<String, CustomLinkedList> loadMessages() {
        try (BufferedReader reader = new BufferedReader(new FileReader(messagesFilePath))) {
            Map<String, CustomLinkedList> messages = new HashMap<>();
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(":");
                String userId = parts[0].trim();
                String[] userMessages = parts[1].split(",");
                CustomLinkedList messageList = new CustomLinkedList();
                for (String userMessage : userMessages) {
                    messageList.add(userMessage.trim());
                }
                messages.put(userId, messageList);
            }
            return messages;
        } catch (FileNotFoundException e) {
            return new HashMap<>();
        } catch (IOException e) {
            e.printStackTrace();
            return new HashMap<>();
        }
    }

    private void saveGraph() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(graphFilePath))) {
            for (Map.Entry<String, CustomLinkedList> entry : adjacencyList.entrySet()) {
                String line = entry.getKey() + ": " + String.join(", ", entry.getValue().toArray());
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveMessages() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(messagesFilePath))) {
            for (Map.Entry<String, CustomLinkedList> entry : messages.entrySet()) {
                String line = entry.getKey() + ": " + String.join(", ", entry.getValue().toArray());
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addUser(String userId) {
        if (!adjacencyList.containsKey(userId)) {
            adjacencyList.put(userId, new CustomLinkedList());
        }
        if (!messages.containsKey(userId)) {
            messages.put(userId, new CustomLinkedList());
        }
        saveGraph();
        saveMessages();
    }

    // public boolean isConnected(String user1, String user2) {
    // Connection connection1 = new Connection(user1, user2);
    // Connection connection2 = new Connection(user2, user1);

    // return connections.contains(connection1) ||
    // connections.contains(connection2);
    // }
    public boolean isConnected(String user1, String user2) {
        CustomLinkedList connectionsUser1 = adjacencyList.get(user1);

        // Check if user2 is in the connections of user1
        if (connectionsUser1 != null) {
            List<String> connectionsList = Arrays.asList(connectionsUser1.toArray());
            return connectionsList.contains(user2);
        }

        return false;
    }

    public void addConnection(String userId1, String userId2) {
        addUser(userId1);
        addUser(userId2);

        adjacencyList.get(userId1).add(userId2);
        adjacencyList.get(userId2).add(userId1);

        connections.add(new Connection(userId1, userId2));
        connections.add(new Connection(userId2, userId1));

        saveGraph();
    }

    public void sendMessage(String senderId, String receiverId, String message) {
        addUser(senderId);
        addUser(receiverId);

        // Send the private message only to the sender and the receiver
        messages.get(senderId).add(receiverId + " -> " + message);
        messages.get(receiverId).add(senderId + " -> " + message);

        // Send the private message to the specified user
        PrintWriter receiverWriter = connectedClients.get(receiverId);
        if (receiverWriter != null) {
            receiverWriter.println(senderId + " -> " + message);
        }

        saveMessages();
    }

    // public String[] receiveMessages(String userId) {
    // addUser(userId);
    // return messages.get(userId).toArray();
    // }

    // public void displayGraph() {
    // for (Map.Entry<String, CustomLinkedList> entry : adjacencyList.entrySet()) {
    // System.out.println("User " + entry.getKey() + ": " + String.join(", ",
    // entry.getValue().toArray()));
    // }
    // }

    public String[] receiveMessages(String userId) {
        addUser(userId);

        CustomLinkedList userMessages = messages.get(userId);
        if (userMessages != null) {
            String[] formattedMessages = new String[userMessages.size()];
            int index = 0;

            for (String message : userMessages.toArray()) {
                // Format the message as "sender: message" and store in the array
                formattedMessages[index++] = userId + ": " + message;
            }

            return formattedMessages;
        } else {
            return new String[0]; // Return an empty array if there are no messages
        }
    }

    public void displayMessages() {
        for (Map.Entry<String, CustomLinkedList> entry : messages.entrySet()) {
            System.out.println(
                    "User " + entry.getKey() + "'s Messages: " + String.join(", ", entry.getValue().toArray()));
        }
    }
    // public Set<String> getConnectedUsers(String userId) {
    // Set<String> connectedUsers = new HashSet<>();

    // // Check if the user is in the graph
    // if (adjacencyList.containsKey(userId)) {
    // connectedUsers.addAll(Arrays.asList(adjacencyList.get(userId).toArray()));
    // }

    // return connectedUsers;
    // }
    public Set<String> getConnectedUsers(String userId) {
        Set<String> connectedUsers = new HashSet<>();

        // Check if the user is in the graph
        if (adjacencyList.containsKey(userId)) {
            connectedUsers.addAll(Arrays.asList(adjacencyList.get(userId).toArray()));
        }

        return connectedUsers;
    }

}
