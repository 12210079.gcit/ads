import java.io.*;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

class UserServer {
    private int port;
    private Map<String, PrintWriter> connectedClients;

    public UserServer(int port) {
        this.port = port;
        this.connectedClients = new HashMap<>();
        startServer();
    }

    // private void startServer() {
    // try (ServerSocket serverSocket = new ServerSocket(port)) {
    // System.out.println("Server started on port " + port);

    // while (true) {
    // Socket clientSocket = serverSocket.accept();
    // System.out.println("New connection: " + clientSocket);
    // new Thread(new ClientHandler(clientSocket)).start();
    // }
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    // }

    private void startServer() {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            System.out.println("Server started on port " + port);

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("New connection: " + clientSocket);
                new Thread(new ClientHandler(clientSocket)).start();
            }
        } catch (BindException e) {
            System.err.println("Error: Port " + port + " is already in use.");
            System.exit(1); // Exit the program
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class ClientHandler implements Runnable {
        private Socket clientSocket;
        private BufferedReader reader;
        private PrintWriter writer;
        private String userId;

        public ClientHandler(Socket clientSocket) {
            this.clientSocket = clientSocket;
            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new PrintWriter(clientSocket.getOutputStream(), true);

                // Get the user's ID from the client
                userId = reader.readLine();
                connectedClients.put(userId, writer);

                System.out.println("User connected: " + userId);
                sendToAll(userId + " joined the chat.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            try {
                String message;
                while ((message = reader.readLine()) != null) {
                    System.out.println("Received message from " + userId + ": " + message);
                    if (message.startsWith(userId + " -> ")) {
                        // Private message format: sender -> receiver: message
                        String[] parts = message.split(" -> ", 2);
                        String receiverId = parts[1].split(":", 2)[0].trim();

                        // Send the private message only to the sender and the receiver
                        sendToUser(userId, message);
                        sendToUser(receiverId, message);
                    } else {
                        // Broadcast the message to all connected clients
                        sendToAll(userId + ": " + message);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                close();
            }
        }

        private void sendToAll(String message) {
            for (PrintWriter clientWriter : connectedClients.values()) {
                clientWriter.println(message);
            }
        }

        private void sendToUser(String userId, String message) {
            PrintWriter userWriter = connectedClients.get(userId);
            if (userWriter != null) {
                userWriter.println(message);
            }
        }

        private void close() {
            try {
                connectedClients.remove(userId);
                sendToAll(userId + " left the chat.");
                System.out.println("User disconnected: " + userId);
                reader.close();
                writer.close();
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
